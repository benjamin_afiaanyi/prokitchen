import Vue from 'vue'
import axios from 'axios'

import x5GMaps from 'x5-gmaps'
// // Option 1: Just your key
// Vue.use(x5GMaps, 'AIzaSyAVTJrXhnabz9zg7K847kT_UQoU2yb0tSI')
// Option 2: With libraries
Vue.use(x5GMaps, { key: 'AIzaSyAVTJrXhnabz9zg7K847kT_UQoU2yb0tSI', libraries: ['places'] })


Vue.prototype.$axios = axios

Vue.prototype.$shipping_url = 'https://staging.proviexpress.com/api/v1/instantCalculator';

axios.defaults.baseURL = `https://server.prokitchen.ng/api/v1/`;
// axios.defaults.baseURL = `http://127.0.0.1:8000/api/v1/`;

import { firestorePlugin } from 'vuefire'

Vue.use(firestorePlugin)


