// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

import 'firebase/storage';


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCpDdufx6F5wUYfkRIr7inB3ci1fAYsMPk",
  authDomain: "prokitchen-3c76a.firebaseapp.com",
  databaseURL: "https://prokitchen-3c76a.firebaseio.com",
  projectId: "prokitchen-3c76a",
  storageBucket: "prokitchen-3c76a.appspot.com",
  messagingSenderId: "930719650523",
  appId: "1:930719650523:web:8438d057c075dfb7ac72fe"
};

let firebaseApp = firebase.initializeApp(firebaseConfig);

let firebaseAuth = firebaseApp.auth();

var storage = firebaseApp.storage();

// const createUser = firebase.functions().httpsCallable('createUser');

firebaseApp.firestore().enablePersistence()
  .catch(function(err) {
    if (err.code == 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
      console.log('Multiple tabs open, persistence can only be enabled')
    } else if (err.code == 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      // ...
      console.log('The current browser does not support all of the features required to enable persistence');
    }
  });
let firebaseDb = firebaseApp.firestore();

firebaseApp.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      unsubscribe();
      resolve(user);
    }, reject);
  })
};

export { firebaseAuth, firebaseDb, storage, firebaseApp }

