import Auth from '../pages/auth/Index';
import Register from '../pages/auth/Register';
import About from '../pages/details/About';
import Terms from '../pages/details/Terms';
import Verify from '../pages/auth/Verify';
import ForgotPass from "pages/auth/ForgotPass";
import ResetPass from "pages/auth/ResetPass";
import Home from "pages/Home";
import setCity from "pages/setCity";
import pageOrders from "pages/orders/Orders";
import pageProfile from "pages/Profile";
import pageProfileEdit from "pages/EditProfile";
import pageAddress from "pages/Address";
import pageFavorites from "pages/Favourites";
import pageNearby from "pages/Nearby";
import ResDetail from 'pages/ResDetail'
import PaymentMethods from "pages/PaymentMethods";
import Quotations from "pages/Quotations";
import BookFood from "pages/book/BookFood";
import OrderInfo from "pages/orders/OrderInfo";
import TrackOrder from "pages/orders/TrackOrder";
import Search from 'pages/book/Search'
import OrderConfirmation from 'pages/orders/OrderConfirmation'
import Profile from 'pages/admin/Profile'
import CreateNewUser from 'pages/admin/CreateNewUser'
import CreateRestaurant from 'pages/admin/CreateRestaurant'
import CreateRes from 'pages/res/CreateRestaurant'
import CreateFood from "pages/res/CreateFood";
import Categories from "pages/admin/Categories";
import ResCategories from "pages/admin/ResCategories";
import Foods from "pages/res/Foods";
import AdminFoods from "pages/admin/Foods";
import HomeNew from "pages/HomeNew";
import Carts from 'pages/Carts'
import Checkout from 'pages/Checkout'
import CatFood from 'pages/CatFood'
import Map from 'pages/Map'
import ResOrders from "pages/res/ResOrders";
import Support from 'pages/Support'
import Share from "pages/Share";
import Payment from "pages/Payment";
import Wallet from 'pages/Wallet'
import FundWallet from 'pages/FundWallet'
import AdminOrders from "pages/admin/AdminOrders";
import EditRestaurant from "pages/res/EditRestaurant";
import Users from 'pages/admin/Users'
import AdminRestaurants from "pages/admin/AdminRestaurants";
import Restaurants from "pages/Restaurants";
import ResFoods from "pages/admin/ResFoods";
import ExportUsers from "pages/admin/ExportUsers";

import AllRes from "pages/admin/AllRestaurants";

const routes = [
  {
    path: '/home',
    component: () => import('layouts/Layout'),
    children: [
      { path: '', component: HomeNew, meta:{name:'Home', hide: true} },
      { path: '/set/city', component: setCity, meta:{name:'Set City'} }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout'),
    children: [
      // { path: '/home', component: HomeNew, meta:{name:'Home', hide: true} },
      { path: '', component: Home, meta:{name:'Home'} }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: 'start', component: () => import('pages/Index.vue') },
      { path: 'loader', component: () => import('pages/Intro2.vue') },
      { path: 'intro/1', component: () => import('pages/intros/1.vue') },
      { path: 'intro/2', component: () => import('pages/intros/2.vue') },
      { path: '/login', component: Auth, meta:{name:'auth'} },
      { path: '/verify', component: Verify, meta:{name:'Verify'} },
      // { path: '/home', component: Home, meta:{name:'Home'} },
      // { path: '/register', component: Register, meta:{name:'Register'} },
    ]
  },

  {
    path: '/',
    component: () => import('layouts/MainLayout'),
    children: [
      // { path: 'home', component: Home, meta:{name:'Home'} },
      { path: '/nearby', component: pageNearby, meta:{name:'Nearby', hide: true} },
      { path: '/map', component: Map, meta:{name:'map'} },
      { path: '/orders', component: pageOrders, meta:{name:'Orders', requiresAuth: true} },
      { path: '/favourites', component: pageFavorites, meta:{name:'Favourites'} },
      { path: '/profile', component: pageProfile, meta:{name:'Profile', requiresAuth: true} },
      { path: '/edit-profile', component: pageProfileEdit, meta:{name:'Edit Profile', requiresAuth: true} },
      { path: '/address', component: pageAddress, meta:{name:'Address', requiresAuth: true} },
      // { path: '/payment-methods', component: PaymentMethods, meta:{name:'Payment Methods'} },
      { path: '/quotations', component: Quotations, meta:{name:'Quotations'} },
      { path: '/book-food/:restaurant', component: BookFood, meta:{name:'Book Food'} },
      { path: '/book/result/:restaurant', component: Search, meta:{name:'Diet Search Result'} },
      { path: '/search/:result', component: Search, meta:{name:'Search'} },
      {path: '/restaurant/:id/:name', component: ResDetail, meta:{name:'Restaurant', hide: true}},
      {path: '/order/details/:id', component: OrderInfo, meta:{name:'Order Details',requiresAuth: true}},
      {path: '/order/track/:id', component: TrackOrder, meta:{name:'Track Order', hide: true,requiresAuth: true}},
      {path: '/:cat/foods', component: CatFood, meta:{name:'Search Result'}},
      {path: '/:cat/restaurants', component: Restaurants, meta:{name:'Restaurants'}},
      {path: '/order/confirmation', component: OrderConfirmation, meta:{name:'Order Confirmation',requiresAuth: true}},
      {path: '/support', component: Support, meta:{name:'Support'}},
      {path: '/share', component: Share, meta:{name:'Share'}},
      {path: '/make/payment', component: Payment, meta:{name:'Payment', requiresAuth: true}},
      {path: '/wallet', component: Wallet, meta:{name:'Wallet', requiresAuth: true}},
      {path: '/fund/wallet', component: FundWallet, meta:{name:'Fund Wallet', requiresAuth: true}},

      //admin side
      {path: '/order', component: OrderConfirmation, meta:{name:'Order Confirmation', requiresAuth: true}},
      {path: '/carts', component: Carts, meta:{name:'Carts', requiresAuth: true}},
      {path: '/checkout', component: Checkout, meta:{name:'Checkout', requiresAuth: true}},
    ]
  },

  {
    path: '/admin',
    component: () => import('layouts/MainLayout'),
    children: [
      // { path: 'profile', component: Profile, meta:{name:'Profile'} },
      { path: 'create/user', component: CreateNewUser, meta:{name:'Create New User',requiresAuth: true,  isAdmin : true} },
      { path: 'create/restaurant', component: CreateRestaurant, meta:{name:'Create Restaurant',requiresAuth: true,  isAdmin : true} },
      { path: 'categories', component: Categories, meta:{name:'Categories',requiresAuth: true,  isAdmin : true} },
      { path: 'res_categories', component: ResCategories, meta:{name:'Restaurant Categories',requiresAuth: true,  isAdmin : true} },
      { path: 'orders', component: AdminOrders, meta:{name:'Admin Orders',requiresAuth: true,  isAdmin : true} },
      { path: 'users', component: Users, meta:{name:'Users List',requiresAuth: true,  isAdmin : true} },
      { path: 'users/export', component: ExportUsers, meta:{name:'Export Users',requiresAuth: true,  isAdmin : true} },
      { path: 'food/list', component: AdminFoods, meta:{name:'Food List',requiresAuth: true,  isAdmin : true} },
      { path: 'restaurants/list', component: AdminRestaurants, meta:{name:'Restaurants List',requiresAuth: true,  isAdmin : true} },
      {path: ':res/foods/:id', component: ResFoods, meta:{name:'Restaurant Foods'}},
      // {path: 'all/res', component: AllRes, meta:{name:'All RES'}},

    ]
  },
  {
    path: '/res',
    component: () => import('layouts/MainLayout'),
    children: [
      { path: 'create/food', component: CreateFood, meta:{name:'Create Food',  isRes : true} },
      { path: 'create/res', component: CreateRes, meta:{name:'Create Restaurant',  isRes : true} },
      { path: 'edit', component: EditRestaurant, meta:{name:'Edit Restaurant',  isRes : true} },
      { path: 'orders', component: ResOrders, meta:{name:' Restaurant Orders',  isRes : true} },
      { path: 'list_foods', component: Foods, meta:{name:'Foods',  isRes : true} },

    ]
  },

  {
    path: '/auth',
    component: () => import('layouts/AuthLayout.vue'),
    children: [
      { path: '/register', component: Register, meta:{name:'Create Account'} },
      { path: '/about', component: About, meta:{name:'About Us'} },
      // { path: '/terms', component: Terms, meta:{name:'Terms And Condition'} },
      { path: '/forgot-password', component: ForgotPass, meta:{name:'Reset Password'} },
      { path: '/terms', component: Terms, meta:{name:'Terms & Condition'} },
      { path: '/reset-pass', component: ResetPass, meta:{name:'Reset Password'} },
    ]
  },


  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];


export default routes
