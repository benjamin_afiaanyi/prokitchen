import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes';

import Flutterwave from 'vue-flutterwave'

import {firebaseApp} from "boot/firebase";
import {ClosePopup, Dialog, Loading} from "quasar";

// Vue.use(Flutterwave, { publicKey: 'FLWPUBK_TEST-99c94e71284b7c3538ba58e2294b7c5a-X' })
Vue.use(Flutterwave, { publicKey: 'FLWPUBK-4087a07d48ccbe87459025221049d287-X' })

Vue.use(VueRouter)


export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  function  logout(){
    store.commit('store/logout');
  }


  Router.beforeEach(async (to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const isAdmin = to.matched.some(record => record.meta.isAdmin);
    const isRes = to.matched.some(record => record.meta.isRes);
    const currentUser = store.state.store.currentUser;
    const admin = currentUser ? (currentUser.is_admin || currentUser.user_type === 'manager') : false;

    Loading.hide()

    if (requiresAuth && !await firebaseApp.getCurrentUser()){
      next('login');
    }else if(to.path === '/login' && await firebaseApp.getCurrentUser()) {
      next('/');
    }else if(isAdmin && !admin) {
      next('/');
    }else if(isRes && !currentUser.is_res) {
      next('/');
    }else if(requiresAuth && !currentUser) {
      logout()
    }else{
      next();
    }
  });

  return Router
}
