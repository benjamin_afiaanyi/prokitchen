import axios from 'axios';

// import {getLocalUser} from "../router";
import { Loading, LocalStorage, Notify } from 'quasar'
import {firebaseAuth, firebaseDb} from "boot/firebase";
const user = [];
const state = {
  currentUser: LocalStorage.getItem("user"),
  currentRes: LocalStorage.getItem("restaurant"),
  currentCity: LocalStorage.getItem("city") ?? 'None',
  currentState: LocalStorage.getItem("state") ?? 'None',
  resItem: LocalStorage.getItem("res_item"),
  featured_cats:[],
  slot_item:[],
  orderDetails : [],
  users:[],
  all_foods:[],
  categories:[],
  order_id:0,
  order:[],
  order_detail:[],
  slides:[],
  featured_products:[],
  best_selling_products:[],
  top_brands:[],
  cat_featured_products:[],
  product:[],
  nearby:[],
  store_info:[],
  hasCart:false,
  pending_orders:[],
  shop_for_me:[],
  has_shown_update:0,
  has_shown_promo:0,
  // isLoggedIn: !user,
  isLoggedIn: !!user,
  loading: false,
  carts:[],
  res_orders:[],
  lists:[],
  wish_lists:[],
  reg_success:'',
  site_details:[],
  new_order:[],
  states:[],
  cities:LocalStorage.getItem("cities"),
  restaurants:[],

  checkoutDetails:[],
  types: [],
  orders:[],
  f_res:[],
  authError: null,
}
const mutations = {

  // remove_cart(state,id){
  //   let index = state.carts.findIndex( (cart) => {
  //     return cart.id == id
  //   });
  //   if(index !== -1) {
  //     state.carts.splice(index, 1);
  //     axios.post("/cart/delete", {'id':id})
  //       .then();
  //   }
  // },

  setUserDetail(state, payload){
    LocalStorage.set("user", payload);
    state.currentUser = payload
  },

  setNewOrder(state, payload){
    state.new_order = payload
  },

  setUserCity(state, payload){
    LocalStorage.set("city", payload);
    state.currentCity = payload
  },
  setUserCities(state, payload){
    LocalStorage.set("cities", payload);
    state.cities = payload
  },
  setUserState(state, payload){
    LocalStorage.set("state", payload);
    state.currentState = payload
  },

  setResDetail(state, payload){
    LocalStorage.set("restaurant", payload);
    state.currentRes = payload
  },

  setResItem(state, payload){
    LocalStorage.set("res_item", payload);
    state.resItem = payload
  },

  setOrderDetails(state, payload){
    state.orderDetails = payload
  },

  updateUpdate(state){
    state.has_shown_update = 1;
  },

  updateAllFoods(state, payload){
    state.all_foods = payload
  },

  updateRestaurants(state, payload){
    state.restaurants = payload
  },

  updateUsers(state, payload) {
    state.users = payload;
  },
  updateStates(state, payload) {
    state.states = payload;
  },

  updateCategories(state, payload) {
    state.categories = payload;
  },

  addList(state,list){
    state.lists.push(list)
  },
  // clearList(state){
  //   state.lists = {};
  // },
  login(state) {
    state.loading = true;
    state.authError = null;
    // state.reg_success = null;
  },
  regSuccess(state, payload){
    state.reg_success = payload
  },
  loginSuccess(state, payload) {
    state.authError = null;
    state.isLoggedIn = true;
    state.loading = false;
    LocalStorage.set("token", payload);
  },
  logout(state) {
    LocalStorage.remove("user");
    LocalStorage.remove("restaurant");
    LocalStorage.remove("res_item");
    state.currentUser = [];
    state.currentRes = null;
    state.isLoggedIn = false;
    firebaseAuth.signOut().then(user => {
      this.$router.push("/login");
    });
  },
  updateFeaturedCats(state, payload) {
    state.featured_cats = payload;
  },

  updateFRes(state, payload) {
    state.f_res = payload;
  },
  update_product(state, payload) {
    state.product = payload;
  },
  update_order_detail(state, payload) {
    state.order_detail = payload;
  },
  update_slot_item(state, payload) {
    state.slot_item = payload;
  },


  updateSlides(state, payload) {
    state.slides = payload;
  },
  updateCatFeaturedProducts(state, payload) {
    state.cat_featured_products = payload;
  },
  updatePendingOrders(state, payload) {
    state.pending_orders = payload;
  },

  updateCheckoutDetails(state, payload) {
    state.checkoutDetails = payload;
  },
  updateWishLists(state, payload) {
    state.wish_lists = payload;
  },
  updateFollowings(state, payload) {
    state.followings = payload;
  },
  updateDetails(state, payload) {
    state.site_details = payload;
  },
  updateCarts(state, payload) {
    state.carts = payload;
  },
  updateOrders(state, payload) {
    state.orders = payload;
  },
  updateResOrders(state, payload) {
    state.res_orders = payload;
  },
  add_cart(state, cart) {
    state.carts.push(cart)
  },

}
const getters = {

  reg_success(state) {
    return state.reg_success;
  },
  types(state) {
    return state.types;
  },

  carts(state){
    return state.carts;
  },
  checkCarts(state){
    return state.hasCart;
  },

  checkout_details(state){
    return state.checkoutDetails;
  },

  // lists(state){
  //   return state.lists;
  // }
}
const actions = {

  loginUser({commit}) {
    commit("login");
  },

  getUsers(context) {
    let users = [];
    firebaseDb.collection("users").get().then
    ((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let item = {}
        item.id = doc.id;
        item.name = doc.data().name+' ('+doc.data().email+')';
        // item.email = doc.data().email;
        users.push(item);
      });
      context.commit('updateUsers', users);
    });
  },

  getCategories(context) {
    let categories = [];
    firebaseDb.collection("categories").get().then
    ((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let item = {}
        item.id = doc.id;
        item.name = doc.data().name;
        item.type = doc.data().type;
        item.is_home = doc.data().is_home ? doc.data().is_home : false;
        // item.image = doc.data().image;
        // item.email = doc.data().email;
        categories.push(item);
      });
      context.commit('updateCategories', categories);
    });
  },

  getAllFoods(context) {
    let all_foods = [];
    firebaseDb.collection("foods").where('userId', '==', state.currentUser.userId).get().then
    ((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        let item = doc.data();
        item.id = doc.id;
        item.name = doc.data().name;
        item.img = doc.data().img;
        item.category = doc.data().category;
        item.price = doc.data().price;
        item.res_address = doc.data().res_address;
        // item.email = doc.data().email;
        all_foods.push(item);
      });
      context.commit('updateAllFoods', all_foods);
    });
  },

  getRestaurants(context) {
    let res = [];
    firebaseDb.collection("restaurants").where('city', '==', state.currentCity).get().then
    ((query) => {
      query.forEach((doc) => {
        let item = {}
        item.id = doc.id;
        item.name = doc.data().name;
        item.img = doc.data().img;
        item.address = doc.data().address;
        item.ratings = doc.data().ratings;
        res.push(item);
      });
      context.commit('updateRestaurants', res);
    });
  },

  getFRess(context) {
    let res = [];
    firebaseDb.collection("restaurants").where('city', '==', state.currentCity)
      .where('featured', '==', true).get().then
    ((query) => {
      query.forEach((doc) => {
        let item = {}
        item.id = doc.id;
        item.name = doc.data().name;
        item.img = doc.data().img;
        item.open_at = doc.data().open_at ?? 7;
        item.close_at = doc.data().close_at ?? 17;
        item.address = doc.data().address;
        item.ratings = doc.data().ratings;
        res.push(item);
      });
      context.commit('updateFRes', res);
    });
  },
  getFRes(context) {
    let res = [];
    let querry = firebaseDb.collection("restaurants").where('city', '==', state.currentCity)
      .where('featured', '==', true);
      console.log(querry);
    querry.get().then(function(doc) {
      if (doc.exists) {
        querry.get().then
        ((query) => {
          query.forEach((doc) => {
            let item = {}
            item.id = doc.id;
            item.name = doc.data().name;
            item.img = doc.data().img;
            item.address = doc.data().address;
            item.ratings = doc.data().ratings;
            res.push(item);
          });
          context.commit('updateFRes', res);
        });
      } else {
        let queries = firebaseDb.collection("restaurants").where('state', '==', state.currentState)
          .where('featured', '==', true);
        queries.get().then
        ((query) => {
          query.forEach((doc) => {
            let item = {}
            item.id = doc.id;
            item.name = doc.data().name;
            item.img = doc.data().img;
            item.address = doc.data().address;
            item.ratings = doc.data().ratings;
            res.push(item);
          });
          context.commit('updateFRes', res);
        });
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
    });
  },

  getCarts(context){
    let carts = [];
    firebaseDb.collection("carts").doc(state.currentUser.userId).collection('details').get().then
    ((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        carts.push(doc.data());
      });
      context.commit('updateCarts', carts);
    });
  },

  getStates(context){
    let states = [];
    firebaseDb.collection("states").get().then
    ((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        states.push(doc.data());
      });
      context.commit('updateStates', states);
    });
  },

  // getOrders(context){
  //   let orders = [];
  //   let order = [];
  //   firebaseDb.collection("orders").where('userId', '==', state.currentUser.userId).get().then
  //   ((querySnapshot) => {
  //     querySnapshot.forEach((doc) => {
  //       this.order = doc.data();
  //       this.order['id'] = doc.id;
  //       orders.push(this.order);
  //     });
  //     context.commit('updateOrders', orders);
  //   });
  // },

  // getResOrders(context){
  //   let orders = [];
  //   let order = [];
  //   firebaseDb.collection("orders").where('res_id', '==', state.currentUser.userId).get().then
  //   ((querySnapshot) => {
  //     querySnapshot.forEach((doc) => {
  //       this.order = doc.data();
  //       this.order['id'] = doc.id;
  //       orders.push(this.order);
  //     });
  //     context.commit('updateResOrders', orders);
  //   });
  // },

  getSlides({commit}) {
    axios.get('/home/sliders')
      .then((response) => {
        commit('updateSlides', response.data);
      })
  },

  addToCart({commit}, payload) {
    if(!state.currentUser){
      this.$router.push("/login");
    }
    return firebaseDb.collection('carts').doc(state.currentUser.userId).collection('details').doc(payload.id).set(payload)
      .then( docRef => {

      }).catch(error => {
        Notify.create({
          type: 'warning',
          color: 'warning',
          timeout: 1000,
          position: 'center',
          message: error
        })
        Loading.hide();
      })
    // this.dispatch('store/getCarts');
  },



  increaseCartQty(payload){
    firebaseDb.collection('carts').doc(state.currentUser.userId).collection('details').doc(payload.id).set(payload)
      .then( docRef => {

      }).catch(error => {
      Notify.create({
        type: 'warning',
        color: 'warning',
        timeout: 1000,
        position: 'center',
        message: error
      })
      Loading.hide();
    })
  },

  getDetails({commit}) {
    axios.post('/site/details')
      .then((response) => {
        commit('updateDetails', response.data);
      })
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

